// RavenSdkModule.java

package com.ravensdk;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.raven.ravenandroidsdk.RavenSdk;
import com.raven.ravenandroidsdk.models.Status;

import android.util.Log;

public class RavenSdkModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public RavenSdkModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RavenSdk";
    }

    @ReactMethod
    public void setUserInformation(String userId, String mobileNumber, String emailAddress) {
        if (userId != null) {
            RavenSdk.INSTANCE.setUser(userId, mobileNumber, null);
        }
    }

    @ReactMethod
    public void setDeviceToken(String token) {
        RavenSdk.INSTANCE.setDeviceToken(token);
    }

    @ReactMethod
    public void updateStatus(String notificationId, String status) {

        if (notificationId != null) {

            //When notification is delivered
            if (status.equals("DELIVERED")) {
                RavenSdk.INSTANCE.updateStatus(notificationId, Status.DELIVERED);
            }

            //When notification is clicked
            if (status.equals("CLICKED")) {
                RavenSdk.INSTANCE.updateStatus(notificationId, Status.CLICKED);
            }

            //When notification is dismissed
            if (status.equals("DISMISSED")) {
                RavenSdk.INSTANCE.updateStatus(notificationId, Status.DISMISSED);
            }
        }
    }

    @ReactMethod
    public void logout() {
        RavenSdk.INSTANCE.logout();
    }

}
