// main index.js

import { NativeModules } from 'react-native';

const { RavenSdk } = NativeModules;

export default RavenSdk;
